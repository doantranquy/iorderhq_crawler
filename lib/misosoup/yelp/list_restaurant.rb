module Misosoup
  module Yelp
    class ListRestaurant < ::Misosoup::Base
      PER_PAGE = 20
      SHOW_INFO_REGEX = %r{(?<=of\s)(?<id>\d+)}
      IMPORT_SOURCE = 'Yelp'
      SOURCE_URL = 'https://www.yelp.ca'

      def url(start_page = 0)
        start_index = start_page * 20
        "https://www.yelp.ca/search?cflt=restaurants&find_loc=Montreal%2C+Quebec%2C+Canada&start=#{start_index}"
      end

      def process
        Enumerator.new do |enum|
          is_finished = false
          current_page = @start_page
          while !is_finished do
            begin
              restaurants = load_page(current_page)
              enum.yield(restaurants, current_page)
              current_page += 1
            rescue Misosoup::OutOfPageRange => e
              is_finished = true
            end
          end
        end
      end

      def load_page(start_page = 0)
        page_url = url(start_page)
        response_page = agent.get(page_url)
        raise Misosoup::OutOfPageRange.new("Page ##{page} is our of range", page) if is_reach_end_page?(start_page, response_page)
        parse_restaurants(response_page)
      end

      private

      def is_reach_end_page?(page, response_page)
        result_info = response_page.at_css('.pagination-results-window').text().strip
        match_data = SHOW_INFO_REGEX.match(result_info)
        match_data && match_data[:id].to_i <= (page * PER_PAGE)
      end

      def parse_restaurants(response_page)
        Enumerator.new do |enum|
          response_page.css('ul.js-search-results li.regular-search-result').each do |restaurant_element|
            begin
              restaurant = parse_restaurant(restaurant_element)
              enum.yield restaurant
            rescue => e
              enum.yield e
            end
          end
        end
      end

      def parse_restaurant(restaurant_element)
        link_element = restaurant_element.at_css('a.biz-name[data-analytics-label="biz-name"]')
        name = link_element.at_css('span')&.text()
        external_path = link_element.attr('href')
        external_id = parse_external_id(external_path)
        address = parse_address(restaurant_element)
        phone_number = restaurant_element.at_css('.biz-phone')&.text()&.strip
        website = get_website_url(external_path)
        return {
            external_link: external_path, external_id: external_id, website: website,
            name: name, address: address, phone_number: phone_number, import_source: IMPORT_SOURCE
        }
      end

      def parse_external_id(path)
        decode_path = URI.decode(path)
        decode_path.split('/').last
      end

      def get_website_url(external_path)
        return unless external_path
        response_data = agent.get("#{SOURCE_URL}#{external_path}")
        biz_website_el = response_data.at_css('.biz-website a')
        if biz_website_el
          link = biz_website_el.attr('href')
          uri = URI.parse(link)
          return CGI::parse(uri.query)['url'].join(',')
        end
      rescue => e
        return nil
      end

      def parse_address(restaurant_element)
        address = restaurant_element.at_css('address')&.text()&.strip
        neighborhood = restaurant_element.at_css('span.neighborhood-str-list')&.text()&.strip
        [address, neighborhood].join(', ')
      end

    end
  end

end