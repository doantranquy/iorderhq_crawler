module Misosoup
  class Base
    def initialize(start_page = 0)
      @start_page = start_page
      @results = []
    end

    def agent
      @agent ||= begin
        agent = ::Mechanize.new
        agent.user_agent_alias = "Mac Safari"
        agent.gzip_enabled = true
        agent.request_headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        }
        agent
      end
    end


  end
end