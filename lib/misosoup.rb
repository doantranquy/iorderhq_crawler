require 'mechanize'
require "misosoup/version"
module Misosoup
  # Your code goes here...
  autoload :OutOfPageRange, 'misosoup/out_of_page_range'
  autoload :Base, 'misosoup/base'
  autoload :PhoneAndWebsite, 'misosoup/phone_and_website'
  autoload :Yp, 'misosoup/yp'
  autoload :Yelp, 'misosoup/yelp'
  autoload :JustEat, 'misosoup/just_eat'
end
