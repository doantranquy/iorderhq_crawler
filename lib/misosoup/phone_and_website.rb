module Misosoup
  class PhoneAndWebsite < Base
    PHONE_NUMBER_REGEX = /(\(?\d{3}\)?[ -]?\d{3}[ -]?\d{4})/

    def initialize(name, address)
      @name = name
      @address = address
    end

    def google_query_url
      query = "#{@name} #{@address}"
      URI.encode("https://www.google.ca/search?q=#{query}&oq=#{query}s&aqs=chrome.0.0l4.1046j0j7&sourceid=chrome&ie=UTF-8")
    end

    def response_page
      @response_page ||= agent.get(google_query_url)
    end

    def get_phone_number
      phone_numbers = response_page.content.scan(PHONE_NUMBER_REGEX)
      if phone_numbers
        phone_numbers.flatten.uniq.join(', ')
      end
    end

    def get_website
      website_el = response_page.at_xpath("//a[text()='Website']")
      if website_el
        website_el.attr('href')
      end
    end
  end

end