module Misosoup
  module Yp
    class ListRestaurant < Misosoup::Base
      END_PAGE_MESSAGE = 'Oops, are we missing your neighbourhood favourite?'
      EXTRACT_EXTERNAL_ID_REGEX = %r{.*/(?<id>\d+)(?=\.html)?}
      IMPORT_SOURCE = 'Yellow Page'

      def url(page = 1)
        "https://www.yellowpages.ca/search/si/#{page}/Restaurants/Montreal+QC"
      end

      def process
        Enumerator.new do |enum|
          is_finished = false
          current_page = @start_page
          while !is_finished do
            begin
              restaurants = load_page(current_page)
              enum.yield(restaurants, current_page)
              current_page += 1
            rescue Misosoup::OutOfPageRange => e
              is_finished = true
            end
          end
        end
      end

      def load_page(page = 1)
        page_url = url(page)
        response_page = agent.get(page_url)
        raise Misosoup::OutOfPageRange.new("Page ##{page} is our of range", page) if is_reach_end_page?(response_page)
        process_data(response_page)
      end

      private

      def is_reach_end_page?(response_page)
        response_page.css('.serp-message.serp-message--warning .missing-business__section--title').text.include?(END_PAGE_MESSAGE)
      end

      def process_data(response_page)
        Enumerator.new do |enum|
          response_page.css('.listing--order').each do |restaurant_element|
            begin
              name = restaurant_element.css('a.listing__name--link.jsListingName').text()
              address = parse_address(restaurant_element)
              phone_number = restaurant_element.css('.jsMapBubblePhone a[data-phone]')&.attr('data-phone')&.value
              website_element_uri = URI.parse(restaurant_element.css('.mlr__item--website a')&.attr('href')&.value) rescue nil
              external_link = restaurant_element.at_css('.listing__name--link.listing__link.jsListingName').attr('href')
              external_id = parse_external_id(external_link)
              if website_element_uri
                website = CGI::parse(website_element_uri.query)['redirect'].join(',')
              end

              enum.yield({name: name, address: address.values.join(', '),
                          external_link: "https://www.yellowpages.ca/#{external_link}",
                          import_source: IMPORT_SOURCE,
                          external_id: external_id, phone_number: phone_number, website: website})
            rescue => e
              enum.yield(e)
            end
          end
        end
      end

      def parse_external_id(external_link)
        match_data = EXTRACT_EXTERNAL_ID_REGEX.match(external_link)
        match_data && match_data[:id]
      end

      def parse_address(restaurant_element)
        address_part_elements = restaurant_element.css('.listing__address--full[itemprop="address"] .jsMapBubbleAddress')
        if address_part_elements
          address_part_elements.inject({}) do |address_obj, element|
            part_name = element.attr('itemprop')
            address_obj[part_name] = element.text()
            address_obj
          end
        end
      end
    end
  end
end