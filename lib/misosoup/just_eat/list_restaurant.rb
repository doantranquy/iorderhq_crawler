module Misosoup
  module JustEat
    class ListRestaurant < ::Misosoup::Base
      IMPORT_SOURCE = 'JustEat'
      attr_accessor :is_last_page

      def url(page = 0)
        "https://www.just-eat.ca/delivery/montreal/?page=#{page}"
      end

      def process
        Enumerator.new do |enum|
          is_finished = false
          current_page = @start_page
          while !is_finished do
            begin
              restaurants = load_page(current_page)
              enum.yield(restaurants, current_page)
              current_page += 1
            rescue Misosoup::OutOfPageRange => e
              is_finished = true
            end
          end
        end
      end

      def load_page(start_page = 0)
        page_url = url(start_page)
        response_page = agent.get(page_url)
        raise Misosoup::OutOfPageRange.new("Page ##{page} is our of range", page) if self.is_last_page
        self.is_last_page = is_reach_end_page?(response_page)
        parse_restaurants(response_page)
      end

      private

      def is_reach_end_page?(response_page)
        response_page.at_css('#SearchResults .pagination .next').nil?
      end

      def parse_restaurants(response_page)
        Enumerator.new do |enum|
          response_page.css('article[data-restaurantid]').each do |restaurant_element|
            begin
              restaurant = parse_restaurant(restaurant_element)
              enum.yield restaurant
            rescue => e
              enum.yield e
            end
          end
        end
      end

      def parse_restaurant(restaurant_element)
        external_id = restaurant_element.attr('data-restaurantid')
        external_path = restaurant_element.at_css('section.restaurantDetails > h3 > a').attr('href')
        name = restaurant_element.at_css('section.restaurantDetails > h3 > a')&.text()&.strip
        address = restaurant_element.at_css('section.restaurantDetails address').text()
        return {
            external_link: external_path, external_id: external_id,
            name: name, address: address, import_source: IMPORT_SOURCE
        }
      end

    end
  end
end