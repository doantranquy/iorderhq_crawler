module Misosoup
  class OutOfPageRange < StandardError
    def initialize(msg="Out of page range", page=nil)
      @page = page
      super(msg)
    end
  end
end